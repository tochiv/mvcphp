<header>
    <div class="logo">
        <a href="/"><span class="cms">MyMVC.com</span></a>
    </div>
    <div class="menu">
        <ul>
            <li class="first active"><a href="/">Profile</a></li>
        </ul>
    </div>
</header>
<div class="mainContainer">
    <div class="homeForm">
        <img src="../assets/images/index.png" alt="" style="height: 150px;">
        <div class="data">
            <h1 class="login">Your login: @<?php echo $login ?></h1>
            Your email: <a target="_blank" href="https://<?php echo $email ?>"><?php echo $email ?></a>
        </div>
        <a href="/auth/logout" class="logout">Выход</a>
    </div>
</div>
<div class="addNews">
    <form class="formNews" method="post" action="/post/addPost" enctype="multipart/form-data">
        <label class="newsText">
            <textarea id="txtar" class="textArea" placeholder="Что у Вас нового?" name="text"></textarea>
        </label>
        <label class="file">
            <img src="../assets/images/picture.png" alt="">
            <input type="file" style="display: none;" name="image">
        </label>
        <button type="submit">Опубликовать</button>
    </form>
</div>

<?php

foreach ($data['list'] as $el) {
    echo '<div class="news">';
        echo "<h1>" . $el->text . "</h1>";
        echo "<img class='postImg' src='$el->image'>";
        echo "<a href='?del=$el->id'>удалить</a>";
    echo '</div>';
}

echo '<div class="pages">';
for ($page = 1; $page <= $data['numberOfPages']; $page++) {
        echo '<a href="?page=' . $page . '">' . $page . '</a> ';
}
echo  '</div>';
?>




