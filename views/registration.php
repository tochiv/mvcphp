<form class="form" action="/auth/registration" method="post">
    <label>Email</label>
    <input type="text" name="email" placeholder="Enter your email">
    <label>Login</label>
    <input type="text" name="login" placeholder="Create a login">
    <label>Password</label>
    <input type="password" name="password" placeholder="Create a password">
    <label>Confirm a password</label>
    <input type="password" name="confirmPassword" placeholder="Confirm a password">
    <button type="submit">Sign Up</button>
    <p>
        Already registered? - <a href="login">Sign In</a>.
    </p>
    <?php
    if ($_SESSION['message']) {
        echo '<p class = msg>' . $_SESSION['message'] . '</p>';
    }
    unset($_SESSION['message']);
    ?>
</form>

