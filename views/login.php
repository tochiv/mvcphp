<form class="form" action="/auth/login" method="post">
    <label>Login</label>
    <input type="text" name="login" placeholder="Enter login">
    <label>Password</label>
    <input type="password" name="password" placeholder="Enter password">
    <button type="submit">Sign In</button>
    <p>
        New user? - <a href="/">Sign Up</a>.
    </p>
    <?php
    if ($_SESSION['message']) {
        echo '<p class = msg>' . $_SESSION['message'] . '</p>';
    }
    unset($_SESSION['message']);
    ?>
</form>
