<?php

namespace controllers;

use core\Controller;
use models\AuthModel;

class AuthController extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new AuthModel();
    }

    public function index()
    {
        // TODO: Implement index() method.
    }

    public function login()
    {
        $this->model->login();
    }

    public function registration()
    {
        $this->model->registration();
    }

    public function logout()
    {
        $this->model->logout();
    }
}