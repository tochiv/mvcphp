<?php

namespace controllers;

use core\Controller;

class LoginController extends Controller
{

    public function index()
    {
        if ($_SESSION['data']) {
            header('Location: /');
        }
        $params = array(
            'title' => 'Login',
            'description' => "Login in MVC",
            'keywords' => array('login', 'mvc'),
            'robots' => 'index'
        );

        $this->loadTemplate("login", $params);
    }
}

