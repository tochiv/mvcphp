<?php

namespace controllers;

use core\Controller;

class RegistrationController extends Controller
{
    public function index()
    {
        if ($_SESSION['data']) {
            header('Location: /');
        }
        $params = array(
            'title' => 'Registration',
            'description' => "A website made using MVC-in-PHP framework",
            'keywords' => array('registration', 'mvc-in-php'),
            'robots' => 'index'
        );

        $this->loadTemplate("registration", $params);
    }
}