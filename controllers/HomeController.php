<?php

namespace controllers;

use core\Controller;
use models\HomeModel;
use models\PostModel;

class HomeController extends Controller
{

    private PostModel $model;

    public function __construct()
    {
        $this->model = new PostModel();
    }

    public function index()
    {

        if (!$_SESSION['data']) {
            header('Location: /registration');
        }

        $page = 1;
        $limit = 5;

        if (isset($_GET['page'])) {
            $page = intval($_GET['page']);
        }

        $params = array(
            'title' => 'Home',
            'description' => "A website made using MVC-in-PHP framework",
            'keywords' => array('home', 'mvc-in-php'),
            'robots' => 'index',
            'login' => $_SESSION['data']['login'],
            'email' => $_SESSION['data']['email'],
            'data' => $this->model->get($_SESSION['data']['id'], $limit, $page),
        );

        $this->loadTemplate("home", $params);
    }

}
