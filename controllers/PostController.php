<?php

namespace controllers;

use core\Controller;
use models\PostModel;

class PostController extends Controller
{
    private PostModel $model;

    public function index()
    {
        // TODO: Implement index() method.
    }

    public function __construct()
    {
        $this->model = new PostModel();
    }

    public function addPost()
    {
        if (!$_SESSION['data']) {
            header('Location: /registration');
        }

        $post = [
            'text' => $_POST['text'],
            'file' => $_FILES['image'],
            'id_user' => $_SESSION['data']['id']
        ];


        $this->model->addPost($post);
    }

}