<?php

namespace models;

use core\Model;
use PDO;

class AuthModel extends Model
{
    public function login()
    {
        $password = $_POST['password'];
        $login = $_POST['login'];
        $password = md5($login . $password);

        $checkStatement = $this->db->prepare("SELECT `id` FROM `auth` WHERE `login` = :login AND `password` = :password");

        $checkStatement->bindParam(':login', $login);
        $checkStatement->bindParam(':password', $password);

        $checkStatement->execute();


        if ($checkStatement->fetch(PDO::FETCH_NUM)) {

            $auth = $this->db->prepare("SELECT * FROM `auth` WHERE `login` = :login AND `password` = :password");

            $auth->bindParam(':login', $login);
            $auth->bindParam(':password', $password);

            $auth->execute();

            $data = $auth->fetch(PDO::FETCH_ASSOC);

            $_SESSION['data'] = [
                "id" => $data['id'],
                "login" => $data['login'],
                "email" => $data['email']
            ];
            header('Location: /home');
        } else {
            $_SESSION['message'] = 'Wrong login or password';
            header('Location: /login');
        }
    }

    public function registration()
    {

        $email = $_POST['email'];
        $login = $_POST['login'];
        $password = $_POST['password'];
        $confirmPassword = $_POST['confirmPassword'];

        if ($password === $confirmPassword) {

            $password = md5($login . $password);
            $myInsertStatement = $this->db->prepare("INSERT INTO auth(email, login, password) VALUES (:email, :login, :password)");

            $myInsertStatement->bindParam(':email', $email);
            $myInsertStatement->bindParam(':login', $login);
            $myInsertStatement->bindParam(':password', $password);

            $myInsertStatement->execute();

            $_SESSION['message'] = 'Registration completed successfully!';
            header('Location: /login');
        } else {
            $_SESSION['message'] = 'Password mismatch';
            header('Location: /registration');
        }
    }

    public function logout()
    {
        unset($_SESSION['data']);
        header('Location: /login');
    }
}