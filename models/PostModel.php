<?php

namespace models;

use core\Model;
use PDO;

class PostModel extends Model
{
    public function get($id_user, $limit, $page)
    {
        $sql = 'SELECT COUNT(id) FROM post WHERE id_user = ?';
        $result = $this->db->prepare($sql);
        $result->execute([$id_user]);
        $result = $result->fetch();

        if(isset($_GET['del'])){
            $id = $_GET['del'];
            $query = "DELETE FROM post WHERE id='$id'";
            $delete = $this->db->prepare($query);
            $delete->execute();

        }

        $numberOfResults = intval($result[0]);

        $numberOfPages = ceil($numberOfResults / $limit);

        $offset = ($page - 1) * $limit;

        $sql = 'SELECT id, image, text, id_user FROM post WHERE id_user = ? ORDER BY id DESC LIMIT ? OFFSET ?';

        $result = $this->db->prepare($sql);

        $result->bindParam(1, $id_user, PDO::PARAM_INT);
        $result->bindParam(2, $limit, PDO::PARAM_INT);
        $result->bindParam(3, $offset, PDO::PARAM_INT);


        $result->execute();
        $result = $result->fetchAll(PDO::FETCH_OBJ);

        return [
            'list' => $result,
            'numberOfPages' => $numberOfPages
        ];

    }

    public function addPost($post)
    {

        $image = "/uploads/images/" . $post['file']['name'];
        $temp = $_FILES['image']['tmp_name'];
        $imageSize = $_FILES['image']['size'];

        if ($imageSize > 0) {
            move_uploaded_file($temp, $_SERVER['DOCUMENT_ROOT'] . $image);
        } else {
            $image = NULL;
        }

        if (!empty($post['text']) || !empty($image)) {

            $insertStatement = $this->db->prepare("INSERT INTO post(image, text, id_user) VALUES(:image, :text, :id_user)");

            $insertStatement->bindParam(':image', $image);
            $insertStatement->bindParam(':text', $post['text']);
            $insertStatement->bindParam(':id_user', $post['id_user']);

            $insertStatement->execute();

            header('Location: /');
        } else {
            header('Location: /');
        }
    }


}